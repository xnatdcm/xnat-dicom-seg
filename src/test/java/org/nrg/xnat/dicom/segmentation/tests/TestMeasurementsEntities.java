/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.tests.TestMeasurementsEntities
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.tests;

import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.framework.services.SerializerService;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.nrg.xnat.dicom.segmentation.entities.Measurements;
import org.nrg.xnat.dicom.segmentation.services.MeasurementsService;
import org.nrg.xnat.dicom.segmentation.tests.configuration.TestMeasurementsEntitiesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestMeasurementsEntitiesConfiguration.class)
@Rollback
@Transactional
public class TestMeasurementsEntities {
    @BeforeClass
    public static void initialize() throws IOException {
        final Resource json = BasicXnatResourceLocator.getResource("classpath:org/nrg/xnat/dicom/segmentation/tests/measurements.json");
        try (final InputStream input = json.getInputStream()) {
            _json = IOUtils.toString(input);
        }
    }

    @Test
    public void testMeasurementsEntity() throws IOException {
        final Measurements original = _service.newEntity();
        original.setImageSessionId("MR1");
        original.setSegmentationImageScanId(100);
        original.setStructuredReportImageScanId(101);
        original.setDicomSegmentationAssessorId("SEG1");
        original.setMeasurements(_serializer.deserializeJson(_json));
        _service.create(original);

        final Measurements retrieved = _service.getSegmentationMeasurements("SEG1");
        assertNotNull(retrieved);
        assertEquals(original, retrieved);
    }

    @Autowired
    private MeasurementsService _service;

    @Autowired
    private SerializerService _serializer;

    private static String _json;
}
