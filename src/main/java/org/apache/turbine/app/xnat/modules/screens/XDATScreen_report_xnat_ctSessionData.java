/*
 * xnat-dicom-seg: org.apache.turbine.app.xnat.modules.screens.XDATScreen_report_xnat_ctSessionData
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.apache.turbine.app.xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xnat.dicom.segmentation.utilities.DataTypeUtils;

public class XDATScreen_report_xnat_ctSessionData extends org.nrg.xdat.turbine.modules.screens.XDATScreen_report_xnat_petSessionData {
    @Override
    public void finalProcessing(RunData data, Context context) {
        super.finalProcessing(data, context);
        DataTypeUtils.checkForUnprocessedSegmentations(context);
    }
}
