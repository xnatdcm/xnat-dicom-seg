/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.repositories.MeasurementsRepository
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.repositories;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnat.dicom.segmentation.entities.Measurements;
import org.springframework.stereotype.Repository;

@Repository
public class MeasurementsRepository extends AbstractHibernateDAO<Measurements> {
    public Measurements getSegmentationMeasurements(final String dicomSegmentationId) {
        return findByUniqueProperty("dicomSegmentationId", dicomSegmentationId);
    }
}
