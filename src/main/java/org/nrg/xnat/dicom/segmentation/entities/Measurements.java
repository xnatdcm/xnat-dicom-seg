/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.entities.Measurements
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.services.SerializerService;
import org.nrg.xdat.om.XdcmDicomsegmentation;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.io.IOException;

/**
 * Contains the JSON-serialized results of converting a DICOM segmentation's associated structured report into a
 * normalized non-DICOM format. Each instance of this entity should reference:
 *
 * <ul>
 *     <li>
 *         A DICOM imaging session object with an ID of an instance of a subclass of the {@link XnatImagesessiondata}
 *         class, e.g. {@link XnatMrsessiondata}.
 *     </li>
 *     <li>
 *         The ID of a scan in the DICOM imaging session object that contains the DICOM segmentation data (i.e. with SEG
 *         modality).
 *     </li>
 *     <li>
 *         The ID of a scan in the DICOM imaging session object that contains the DICOM structured report data (i.e.
 *         with SR modality).
 *     </li>
 *     <li>
 *         A DICOM segmentation assessor object with the ID of an instance of the {@link XdcmDicomsegmentation} data
 *         type through the {@link #getDicomSegmentationAssessorId() DICOM segmentation ID} property.
 *     </li>
 *     <li>
 *         The extracted measurement data from the structured report as JSON text or a <b>JsonNode</b>.
 *     </li>
 * </ul>
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "dicomSegmentationAssessorId"), @UniqueConstraint(columnNames = {"imageSessionId", "segmentationImageScanId", "structuredReportImageScanId"})})
public class Measurements extends AbstractHibernateEntity {
    /**
     * Creates a new empty measurements object.
     */
    public Measurements() {
        super();
    }

    /**
     * Creates a new measurements object related to the indicated data objects.
     *
     * @param imageSessionId              The ID of the source image session.
     * @param segmentationImageScanId     The ID of the scan in the source image session that contains the DICOM segmentation data.
     * @param structuredReportImageScanId The ID of the scan in the source image session that contains the structured report (measurement) data.
     * @param dicomSegmentationAssessorId The ID of the DICOM segmentation assessor.
     * @param measurements                The measurements extracted from the structured report.
     */
    public Measurements(final String imageSessionId, final int segmentationImageScanId, final int structuredReportImageScanId, final String dicomSegmentationAssessorId, final JsonNode measurements) {
        _imageSessionId = imageSessionId;
        _segmentationImageScanId = segmentationImageScanId;
        _structuredReportImageScanId = structuredReportImageScanId;
        _dicomSegmentationAssessorId = dicomSegmentationAssessorId;
        _measurements = measurements;
    }

    /**
     * Gets the ID of the source image session. This always refers to an instance of a subclass of the {@link
     * XnatImagesessiondata} class, e.g. {@link XnatMrsessiondata}.
     *
     * @return The ID of the source image session.
     */
    public String getImageSessionId() {
        return _imageSessionId;
    }

    /**
     * Sets the ID of the source image session. This should always refer to an instance of a subclass of the {@link
     * XnatImagesessiondata} class, e.g. {@link XnatMrsessiondata}.
     *
     * @param imageSessionId The ID of the source image session.
     */
    public void setImageSessionId(final String imageSessionId) {
        _imageSessionId = imageSessionId;
    }

    /**
     * Gets the ID of the scan in the source image session that contains the DICOM segmentation data.
     *
     * @return The ID of the DICOM segmentation scan in the source image.
     */
    public int getSegmentationImageScanId() {
        return _segmentationImageScanId;
    }

    /**
     * Sets the ID of the scan in the source image session that contains the DICOM segmentation data.
     *
     * @param segmentationImageScanId  The ID of the DICOM segmentation scan in the source image.
     */
    public void setSegmentationImageScanId(final int segmentationImageScanId) {
        _segmentationImageScanId = segmentationImageScanId;
    }

    /**
     * Gets the ID of the scan in the source image session that contains the structured report (measurement) data.
     *
     * @return The ID of the DICOM structured report scan in the source image.
     */
    public int getStructuredReportImageScanId() {
        return _structuredReportImageScanId;
    }

    /**
     * Sets the ID of the scan in the source image session that contains the DICOM structured report data.
     *
     * @param structuredReportImageScanId The ID of the DICOM structured report scan in the source image.
     */
    public void setStructuredReportImageScanId(final int structuredReportImageScanId) {
        _structuredReportImageScanId = structuredReportImageScanId;
    }

    /**
     * Gets the ID of the DICOM segmentation assessor. This always refers to an instance of the {@link
     * XdcmDicomsegmentation} class.
     *
     * @return The ID of the DICOM segmentation assessor.
     */
    public String getDicomSegmentationAssessorId() {
        return _dicomSegmentationAssessorId;
    }

    /**
     * Sets the ID of the DICOM segmentation assessor. This should always refer to an instance of the {@link
     * XdcmDicomsegmentation} class.
     *
     * @param dicomSegmentationAssessorId The ID of the DICOM segmentation assessor.
     */
    public void setDicomSegmentationAssessorId(final String dicomSegmentationAssessorId) {
        _dicomSegmentationAssessorId = dicomSegmentationAssessorId;
    }

    @Type(type = "com.marvinformatics.hibernate.json.JsonUserType")
    public JsonNode getMeasurements() {
        return _measurements;
    }

    public void setMeasurements(final JsonNode measurements) {
        _measurements = measurements;
    }

    @Transient
    @JsonIgnore
    public String getMeasurementsAsJson() {
        return _measurements.asText();
    }

    @Transient
    @JsonIgnore
    public void setMeasurementsAsJson(final SerializerService serializer, final String json) throws IOException {
        _measurements = serializer.deserializeJson(json);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Measurements)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        final Measurements that = (Measurements) o;

        return getStructuredReportImageScanId() == that.getStructuredReportImageScanId() &&
               getSegmentationImageScanId() == that.getSegmentationImageScanId() &&
               StringUtils.equals(getDicomSegmentationAssessorId(), that.getDicomSegmentationAssessorId()) &&
               StringUtils.equals(getImageSessionId(), that.getImageSessionId()) &&
               getMeasurements().equals(that.getMeasurements());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getDicomSegmentationAssessorId().hashCode();
        result = 31 * result + getImageSessionId().hashCode();
        result = 31 * result + getStructuredReportImageScanId();
        result = 31 * result + getSegmentationImageScanId();
        result = 31 * result + getMeasurements().hashCode();
        return result;
    }

    private String   _dicomSegmentationAssessorId;
    private String   _imageSessionId;
    private int      _structuredReportImageScanId;
    private int      _segmentationImageScanId;
    private JsonNode _measurements;
}
