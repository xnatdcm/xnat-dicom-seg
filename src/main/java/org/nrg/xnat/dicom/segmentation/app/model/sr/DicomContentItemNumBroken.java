/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomContentItemNumBroken
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.xnat.dicom.segmentation.app.model.DicomCodeValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomContentItemNumBroken extends DicomContentItem {
    private String value;
    private DicomCodeValue unitsCode;
    private List<DicomCodeValue> qualifierCodes;

    public DicomContentItemNumBroken(DicomObject dicomObject, String relationship){
        super( dicomObject, relationship);
        DicomElement mvs = dicomObject.get( Tag.MeasuredValueSequence);
        if( mvs.countItems() == 1) {
            DicomObject item = mvs.getDicomObject(0);
            value = item.getString(( Tag.NumericValue));
            unitsCode = new DicomCodeValue( item, Tag.MeasurementUnitsCodeSequence);
        }
        else {
            value = null;
        }
        qualifierCodes = new ArrayList<>();
        // qualifier Codes should be in Tag.NumericValueQualifierCodeSequence
        // not in Tag.ContentSequence.  Content Sequence is only for items of type CONTAINER only.
        DicomElement qcs = dicomObject.get( Tag.ContentSequence);
        if( qcs != null) {
            for (int i = 0; i < qcs.countItems(); i++) {
                qualifierCodes.add(new DicomCodeValue(qcs.getDicomObject(i)));
            }
        }
    }

    public String getValue() {
        return value;
    }

    public DicomCodeValue getUnitsCode() {
        return unitsCode;
    }

    public List<DicomCodeValue> getQualifierCodes() {
        return qualifierCodes;
    }

    public String toString() {
        StringBuilder sb1 = new StringBuilder( super.toString());
        StringBuilder sb = new StringBuilder();
        sb.append(" Value: '" + value + "' Units: '" + unitsCode + "' Qualifiers: [");
        for( int i = 0; i < qualifierCodes.size(); i++) {
            sb.append("[" + qualifierCodes.get(i) + "]");
        }
        int start = sb1.indexOf("<replace>");
        int stop = start + "<replace>".length();
        sb1.replace( start, stop, sb.toString());
        sb.append("]\n");
        return sb1.toString();
    }

}
