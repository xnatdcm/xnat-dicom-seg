/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomContentItemComposite
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomContentItemComposite extends DicomContentItem {
    private String referencedSOPClassUID;
    private String referencedSOPInstanceUID;

    public DicomContentItemComposite(DicomObject dicomObject, String relationship) {
        super( dicomObject, relationship);
        DicomElement refSOPSeqElement = dicomObject.get( Tag.ReferencedSOPSequence);
        DicomObject item = refSOPSeqElement.getDicomObject();   // only one item shall be included.
        referencedSOPClassUID = item.getString( Tag.ReferencedSOPClassUID);
        referencedSOPInstanceUID = item.getString( Tag.ReferencedSOPInstanceUID);
    }

    public String getReferencedSOPClassUID() {
        return referencedSOPClassUID;
    }

    public String getReferencedSOPInstanceUID() {
        return referencedSOPInstanceUID;
    }


    public String toString() {
        StringBuilder sb1 = new StringBuilder( super.toString());
        StringBuilder sb = new StringBuilder();
        sb.append(" SOP Class: '" + referencedSOPClassUID + "' SOPInstanceUID: '" + referencedSOPInstanceUID + "'");
        int start = sb1.indexOf("<replace>");
        int stop = start + "<replace>".length();
        sb1.replace( start, stop, sb.toString());
        sb.append("]\n");
        return sb1.toString();
    }
}
