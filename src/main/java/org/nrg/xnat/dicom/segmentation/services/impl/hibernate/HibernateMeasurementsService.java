/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.services.impl.hibernate.HibernateMeasurementsService
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.services.impl.hibernate;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.dicom.segmentation.entities.Measurements;
import org.nrg.xnat.dicom.segmentation.repositories.MeasurementsRepository;
import org.nrg.xnat.dicom.segmentation.services.MeasurementsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class HibernateMeasurementsService extends AbstractHibernateEntityService<Measurements, MeasurementsRepository> implements MeasurementsService {
    @Override
    @Transactional
    public Measurements getSegmentationMeasurements(final String dicomSegmentationId) {
        return getDao().getSegmentationMeasurements(dicomSegmentationId);
    }
}
