/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomContentItemContainer
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.nrg.xnat.dicom.segmentation.app.model.DicomCodeValue;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomContentItemContainer extends DicomContentItem {
    private String continuityOfContent;
    private DicomCodeValue contentTemplate;
    private String completionFlag;
    private String verificationFlag;
    private String contentDate;
    private String contentTime;

    public DicomContentItemContainer(DicomObject dicomObject, String relationship){
        super( dicomObject, relationship);
        continuityOfContent = dicomObject.getString( 0x0040A050);
        contentTemplate = getContentTemplate( dicomObject, 0x0040A504);
        completionFlag = dicomObject.getString( 0x0040A491);
        verificationFlag = dicomObject.getString( 0x0040A493);
        contentDate = dicomObject.getString( 0x00080023);
        contentTime = dicomObject.getString( 0x00080033);
    }

    public String getCompletionFlag() {
        return completionFlag;
    }

    public String getVerificationFlag() {
        return verificationFlag;
    }

    public String getContentDate() {
        return contentDate;
    }

    public String getContentTime() {
        return contentTime;
    }

    public String getContinuityOfContent() {
        return continuityOfContent;
    }

    public DicomCodeValue getContentTemplate() {
        return contentTemplate;
    }

    public String toString() {
        StringBuilder sb1 = new StringBuilder( super.toString());
        StringBuilder sb = new StringBuilder();
        sb.append(" continuityOfContent: " + continuityOfContent);
        sb.append(" content template: " + contentTemplate );
        if( completionFlag != null) sb.append(" completionFlag: " + completionFlag );
        if( verificationFlag != null) sb.append(" verificationFlag: " + verificationFlag );
        if( contentDate != null) sb.append(" contentDate: " + contentDate );
        if( contentTemplate != null) sb.append(" contentTime: " + contentTime );
        int start = sb1.indexOf("<replace>");
        int stop = start + "<replace>".length();
        sb1.replace( start, stop, sb.toString());
        sb.append("]\n");
        return sb1.toString();
    }

    private DicomCodeValue getContentTemplate(DicomObject dicomObject, int tag) {
        DicomCodeValue dicomCodeValue = null;
        DicomElement de = dicomObject.get( tag);
        if( de != null && de.hasItems()) {
            DicomObject item = de.getDicomObject(0);
            dicomCodeValue = new DicomCodeValue( item.getString(0x00080105), item.getString(0x00080118), item.getString(0x0040DB00));
        }
        return dicomCodeValue;
    }

}
