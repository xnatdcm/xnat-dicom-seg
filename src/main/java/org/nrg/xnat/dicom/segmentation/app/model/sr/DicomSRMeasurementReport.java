/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomSRMeasurementReport
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.nrg.xnat.dicom.segmentation.app.model.DicomCodeValue;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomSRMeasurementReport {
    private static final DicomCodeValue contentTemplate = new DicomCodeValue( "DCMR", "1.2.840.10008.8.1.1", "1500");

    private DicomContentItem rootItem;

    // TID 1500 measurement reports should have Concept names from  DCID 7021 "Measurement Report Document Titles"
    private DicomCodeValue conceptName;

    // includes a concept of Language from DTID 1204 "Language of Content Item and Descendents"
    private DicomCodeValue language;

    // includes obs context "Observation Context" from DTID 1001

    // has 1-n procedures reported with concept names EV(121058, DCM, "Procedure Reported") and values from BCID 100 "Quantitative Diagnostic Imaging Procedures"
    List<DicomCodeValue> procedureReported;

    // includes container with name DTID 1600 "Image Library"
//    private ImageLibrary;

    // includes one of container with name EV(126010,DCM,"Imaging Measurements")
    // or EV(126011,DCM."Derived Imaging Measurements")
    // or container with name EV(C0034375,UMLS,"Qualitative Evaluations")

    public DicomSRMeasurementReport(DicomObject dicomObject) {
        if( hasTemplate( dicomObject) ) {
            rootItem = extractContent( dicomObject);

        }
        else {
            StringBuilder sb = new StringBuilder();
            sb.append("Mismatched templates.\n");
            sb.append("Expect Content Template: " + contentTemplate + "\n");
            sb.append("Provided Content Template: " + getContentTemplate(dicomObject) + "\n");
            throw new IllegalArgumentException( sb.toString());
        }
    }

    private DicomCodeValue getContentTemplate(DicomObject dicomObject) {
        DicomCodeValue dicomCodeValue = null;
        DicomElement de = dicomObject.get(0x0040A504);
        if( de != null && de.hasItems()) {
            DicomObject item = de.getDicomObject(0);
            dicomCodeValue = new DicomCodeValue( item.getString(0x00080105), item.getString(0x00080118), item.getString(0x0040DB00));
        }
        return dicomCodeValue;
    }

    public boolean hasTemplate( DicomObject dicomObject) {
        String modality = dicomObject.getString( 0x00080060);
        return ("SR".equals(modality) && contentTemplate.equals( getContentTemplate( dicomObject)));
    }

    public DicomContentItem extractContent( DicomObject dicomObject) {
        DicomContentItem item = DicomContentItem.createInstance( dicomObject, null);
        return item;
    }

    public String getTitle() {
        return "The title goes here.";
    }

    public void write(PrintStream out, String format) throws JsonProcessingException {
        switch (format) {
            case "text":
                out.println( rootItem);
                break;
            case "json":
                ObjectMapper mapper = new ObjectMapper();
                out.println( mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootItem));
                break;
            default:
                break;
        }
    }
}
