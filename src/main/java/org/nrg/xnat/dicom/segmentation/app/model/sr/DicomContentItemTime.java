/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomContentItemTime
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomContentItemTime extends DicomContentItem {
    private String time;

    public DicomContentItemTime(DicomObject dicomObject, String relationship){
        super( dicomObject, relationship);
        DicomElement conceptCode = dicomObject.get( Tag.ConceptCodeSequence);
        time = dicomObject.getString( Tag.Time);
    }

    public String getTime() {
        return time;
    }

    public String toString() {
        StringBuilder sb1 = new StringBuilder( super.toString());
        StringBuilder sb = new StringBuilder();
        sb.append(" Value: " + time );
        int start = sb1.indexOf("<replace>");
        int stop = start + "<replace>".length();
        sb1.replace( start, stop, sb.toString());
        sb.append("]\n");
        return sb1.toString();
    }

}
