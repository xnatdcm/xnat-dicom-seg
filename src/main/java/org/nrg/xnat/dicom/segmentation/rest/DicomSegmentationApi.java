/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.rest.DicomSegmentationApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import io.swagger.annotations.*;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.om.XdcmDicomsegmentation;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImageassessordata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnat.dicom.segmentation.entities.Measurements;
import org.nrg.xnat.dicom.segmentation.services.MeasurementsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nullable;
import java.util.List;

@Api(description = "XNAT DICOM Segmentation API")
@XapiRestController
@RequestMapping(value = "/dicomseg")
public class DicomSegmentationApi extends AbstractXapiRestController {
    @Autowired
    public DicomSegmentationApi(final MeasurementsService service, final UserManagementServiceI userManagementService, final RoleHolder roleHolder) {
        super(userManagementService, roleHolder);
        _service = service;
    }

    @ApiOperation(value = "Get list of all DICOM segmentation assessors associated with the indicated DICOM imaging session.", notes = "The list returned contains the IDs of all of the associated assessors.", response = String.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Returns a list of DICOM segmentation assessors associated with the indicated DICOM imaging session."),
                   @ApiResponse(code = 204, message = "The specified experiment was found but there were no associated segmentation assessors."),
                   @ApiResponse(code = 400, message = "The specified experiment was found but was not an imaging session."),
                   @ApiResponse(code = 404, message = "The specified experiment was not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{sessionId}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> getDicomSegmentationAssessorIds(@ApiParam(value = "ID of the DICOM study for which you want to retrieve associated DICOM segmentation assessors", required = true) @PathVariable("sessionId") final String sessionId) {
        final XnatExperimentdata experiment = XnatImagesessiondata.getXnatExperimentdatasById(sessionId, getSessionUser(), false);
        if (experiment == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!XnatImagesessiondata.class.isAssignableFrom(experiment.getClass())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final XnatImagesessiondata        image     = (XnatImagesessiondata) experiment;
        final List<XnatImageassessordata> assessors = image.getAssessors(XdcmDicomsegmentation.SCHEMA_ELEMENT_NAME);
        if (assessors.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(Lists.transform(assessors, ASSESSOR_ID_TRANSFORM), HttpStatus.OK);
    }

    @ApiOperation(value = "Gets the DICOM segmentation measurements object from the path variables.", notes = "The form parameters accepted for this function are segmentationImageScanId (the scan ID with DICOM SEG modality), structuredReportImageScanId (the scan ID with DICOM SR modality containing the DICOM segmentation measurements), dicomSegmentationAssessorId (the XNAT ID of the image assessor instance containing the processed DICOM segmentation data), and measurements (the data extracted from the structured report). The measurements instance will be associated with the imaging session specified by the sessionId path variable.", response = Measurements.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Returns the requested measurements object."),
                   @ApiResponse(code = 400, message = "The specified experiment was found but was not an imaging session or the specified scan IDs were invalid."),
                   @ApiResponse(code = 404, message = "The specified experiment was not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{dicomSegmentationAssessorId}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Measurements> getMeasurements(@ApiParam("The XNAT ID of the image assessor instance containing the processed DICOM segmentation data") @PathVariable final String dicomSegmentationAssessorId) {
        return new ResponseEntity<>(_service.getSegmentationMeasurements(dicomSegmentationAssessorId), HttpStatus.OK);
    }

    @ApiOperation(value = "Creates a new DICOM segmentation measurements object from the request body for the specified segmentation object.", notes = "The measurements payload is set to the data extracted from the structured report and posted in the request body. The measurements instance will be associated with the segmentation object and related imaging session specified in the path.", response = Measurements.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Returns the newly created measurements object."),
                   @ApiResponse(code = 400, message = "The specified experiment was found but was not an imaging session or the specified scan IDs were invalid."),
                   @ApiResponse(code = 404, message = "The specified experiment was not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{dicomSegmentationAssessorId}/{structuredReportImageScanId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Measurements> createMeasurements(@ApiParam("The XNAT ID of the image assessor instance containing the processed DICOM segmentation data") @PathVariable final String dicomSegmentationAssessorId,
                                                           @ApiParam("The scan ID with DICOM SR modality containing the DICOM segmentation measurements") @PathVariable final int structuredReportImageScanId,
                                                           @ApiParam("The data extracted from the structured report in JSON format") @RequestBody final JsonNode measurements) {
        final XdcmDicomsegmentation segmentation = XdcmDicomsegmentation.getXdcmDicomsegmentationsById(dicomSegmentationAssessorId, getSessionUser(), false);
        if (segmentation == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final String sessionId               = segmentation.getImageSessionData().getId();
        final int    segmentationImageScanId = segmentation.getSegmentid();

        final Measurements instance = _service.create(sessionId, segmentationImageScanId, structuredReportImageScanId, dicomSegmentationAssessorId, measurements);
        return new ResponseEntity<>(instance, HttpStatus.OK);
    }

    private static final Function<XnatImageassessordata, String> ASSESSOR_ID_TRANSFORM = new Function<XnatImageassessordata, String>() {
        @Nullable
        @Override
        public String apply(@Nullable final XnatImageassessordata input) {
            if (input == null) {
                return null;
            }
            return input.getId();
        }
    };

    private final MeasurementsService _service;
}
