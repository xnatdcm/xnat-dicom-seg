/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.seg.DicomSegmentation
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.seg;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UIDDictionary;
import org.nrg.xnat.dicom.segmentation.app.model.DicomCodeValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by davidmaffitt on 11/19/16.
 */
public class DicomSegmentation {
    private String studyUID;
    private String seriesUID;
    private String sopInstanceUID;
    private int seriesNumber;

    // Segmentation Image Module
    private String segmentationType;
    private String segmentationFractionalType;
    private String maximumFractionalValue;

    // Seg Image Module - Segment Sequence
    private int            segmentationNumber;
    private String         segmentLabel;
    private String         segmentDescription;
    private String         segmentAlgorithmType;
    private DicomCodeValue anatomicRegionCode;
    private DicomCodeValue segmentedPropertyCategoryCode;
    private DicomCodeValue segmentedPropertyTypeCode;
    private DicomCodeValue segmentedPropertyTypeModifierCode;
    private String         trackingUID;
    private String         trackingID;
    private String         segmentAlgorithmName;
//    private String recommendedDisplayGrayscaleValue;
//    private String recommendedDisplayCIELabValue;

    // Functional Groups
    private String instanceNumber;
    private String contentDate;
    private String contentTime;
    private int numberOfFrames;

    // Functional Groups - Pixel measures  type 1
//    private String pixelSpacing;     // type 1C
//    private String sliceThickness;   // type 1C
//    private String spacingBetweenSlices;  // type 3

    // Functional Groups - Frame Content  type 1

    // Functional Groups - Pixel measures  type 1
    
    private List<String> referencedStudyUIDs;
    private List<String> referencedSeriesUIDs;
    private List<String> referencedModalities;


    public static List<DicomSegmentation> createSegmentations( DicomObject dicomObject) {
        List<DicomSegmentation> segmentations = new ArrayList<>();
        int segmentCount = segmentCount( dicomObject);
        for( int i = 0; i < segmentCount; i++) {
            segmentations.add( new DicomSegmentation( dicomObject, i));
        }

        return segmentations;
    }

    private DicomSegmentation(DicomObject dicomObject, int segmentIndex) {
        studyUID = dicomObject.getString(Tag.StudyInstanceUID);
        seriesUID = dicomObject.getString( Tag.SeriesInstanceUID);
        sopInstanceUID = dicomObject.getString( Tag.SOPInstanceUID);
        seriesNumber = dicomObject.getInt( Tag.SeriesNumber);
        instanceNumber = dicomObject.getString( Tag.InstanceNumber);
        contentDate = dicomObject.getString( Tag.ContentDate);
        contentTime = dicomObject.getString( Tag.ContentTime);
        
        segmentationType = segmentationType( dicomObject);
        segmentationFractionalType = segmentationFractionalType( dicomObject);
        maximumFractionalValue = maximumFractionalValue( dicomObject);

        segmentationNumber = segmentationNumber( dicomObject, segmentIndex);
        segmentLabel = segmentLabel( dicomObject, segmentIndex);
        segmentDescription = segmentDescription( dicomObject, segmentIndex);
        segmentAlgorithmType = segmentAlgorithmType( dicomObject, segmentIndex);
        segmentAlgorithmName = segmentAlgorithmName( dicomObject, segmentIndex);
        trackingID = trackingID( dicomObject, segmentIndex);
        trackingUID = trackingUID( dicomObject, segmentIndex);

        anatomicRegionCode = anatomicRegionCode( dicomObject, segmentIndex);
        segmentedPropertyCategoryCode = segmentedPropertyCategoryCode( dicomObject, segmentIndex);
        segmentedPropertyTypeCode = segmentedPropertyTypeCode( dicomObject, segmentIndex);
        segmentedPropertyTypeModifierCode = segmentedPropertyTypeModifierCode( dicomObject, segmentIndex);
//        recommendedDisplayCIELabValue = recommendedDisplayCIELabValue( dicomObject, segmentIndex);

        referencedStudyUIDs = referencedStudyUIDs( dicomObject, segmentIndex);
        referencedSeriesUIDs = referencedSeriesUIDs( dicomObject, segmentIndex);
        referencedModalities = referencedModalities( dicomObject, segmentIndex);

    }

    private int segmentationNumber(DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getInt( Tag.SegmentNumber);
    }

    private String segmentLabel(DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getString( Tag.ContentLabel);
    }

    private String segmentDescription(DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getString( Tag.ContentDescription);
    }

    private String segmentAlgorithmType( DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getString( Tag.SegmentAlgorithmType);
    }

    private String segmentAlgorithmName( DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getString( Tag.SegmentAlgorithmName);
    }

    private String trackingID(DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getString( 0x00620020);
    }

    private String trackingUID(DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getString( 0x00620021);
    }

    private static int segmentCount(DicomObject dicomObject) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.countItems();
    }

    private String recommendedDisplayCIELabValue( DicomObject dicomObject, int segmentIndex) {
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        return de.getDicomObject( segmentIndex).getString( Tag.RecommendedDisplayCIELabValue);
    }

    private DicomCodeValue anatomicRegionCode( DicomObject dicomObject, int segmentIndex) {
        DicomCodeValue dcv = null;
        DicomElement de = dicomObject.get( Tag.SegmentSequence);
        DicomElement ars = de.getDicomObject(segmentIndex).get( Tag.AnatomicRegionSequence);
        if( ars != null)
            dcv = new DicomCodeValue( ars.getDicomObject(0));
        return dcv;
    }

    private DicomCodeValue segmentedPropertyCategoryCode( DicomObject dicomObject, int segmentIndex) {
        return new DicomCodeValue( dicomObject.get( Tag.SegmentSequence).getDicomObject( segmentIndex), Tag.SegmentedPropertyCategoryCodeSequence);
    }

    private DicomCodeValue segmentedPropertyTypeCode( DicomObject dicomObject, int segmentIndex) {
        return new DicomCodeValue( dicomObject.get( Tag.SegmentSequence).getDicomObject( segmentIndex), Tag.SegmentedPropertyTypeCodeSequence);
    }

    private DicomCodeValue segmentedPropertyTypeModifierCode( DicomObject dicomObject, int segmentIndex) {
//        return new DicomCodeValue( dicomObject.get( Tag.SegmentSequence).getDicomObject( segmentIndex).get(0x00620011).getDicomObject(0));
        return null;
    }

    private String segmentationType( DicomObject dicomObject) {
        return dicomObject.getString( Tag.SegmentationType);
    }

    private String segmentationFractionalType( DicomObject dicomObject) {
        return dicomObject.getString( Tag.SegmentationFractionalType);
    }

    private String maximumFractionalValue( DicomObject dicomObject) {
        return dicomObject.getString( Tag.MaximumFractionalValue);
    }

    private String pixelSpacing( DicomObject dicomObject, int frameIndex) {
        String ps = null;
        DicomElement de = dicomObject.get( Tag.SharedFunctionalGroupsSequence);
        DicomObject dobj = de.getDicomObject();
        DicomElement pms = dobj.get( Tag.PixelMeasuresSequence);
        if( pms != null) {
            ps = dobj.getString( Tag.PixelSpacing);
            return ps;
        }

        de = dicomObject.get( Tag.PerFrameFunctionalGroupsSequence);
        dobj = de.getDicomObject( frameIndex);
        pms = dobj.get( Tag.PixelMeasuresSequence);
        if( pms != null) {
            ps = dobj.getString( Tag.PixelSpacing);
        }
        return ps;
    }

    private List<String> referencedStudyUIDs( DicomObject dicomObject, int segmentIndex) {
        List<String> studyUIDs = new ArrayList<>();

        DicomElement de = dicomObject.get( Tag.ReferencedSeriesSequence);
        if( de != null) {
            studyUIDs.add(dicomObject.getString(Tag.StudyInstanceUID));
        }

        de = dicomObject.get( Tag.StudiesContainingOtherReferencedInstancesSequence);
        if( de != null) {
            for( int i = 0; i < de.countItems(); i++) {
                studyUIDs.add( de.getDicomObject(i).getString( Tag.StudyInstanceUID));
            }
        }

        return studyUIDs;
    }

    private List<String> referencedSeriesUIDs( DicomObject dicomObject, int frameNumber) {
        List<String> seriesUIDs = new ArrayList<>();
        DicomElement de = dicomObject.get( Tag.ReferencedSeriesSequence);

        for( int i = 0; i < de.countItems(); i++) {
            seriesUIDs.add( de.getDicomObject(i).getString( Tag.SeriesInstanceUID));
        }

        de = dicomObject.get( Tag.StudiesContainingOtherReferencedInstancesSequence);
        if( de != null) {
            for( int istudy = 0; istudy < de.countItems(); istudy++) {
                DicomElement seriesSeqElemnt = de.getDicomObject(istudy).get( Tag.ReferencedSeriesSequence);
                for( int iseries = 0; iseries < seriesSeqElemnt.countItems(); iseries++) {
                    seriesUIDs.add( seriesSeqElemnt.getDicomObject(iseries).getString( Tag.SeriesInstanceUID));
                }
            }
        }

        return seriesUIDs;
    }

    private List<String> referencedModalities( DicomObject dicomObject, int frameNumber) {
        Set<String> modalities = new HashSet<>();
        DicomElement de = dicomObject.get( Tag.ReferencedSeriesSequence);

        for( int i = 0; i < de.countItems(); i++) {
            DicomElement seriesElement = de.getDicomObject(i).get( Tag.ReferencedInstanceSequence);
            String sopClassUID = seriesElement.getDicomObject(0).getString( Tag.ReferencedSOPClassUID);
            modalities.add(UIDDictionary.getDictionary().nameOf( sopClassUID));
        }

        return new ArrayList<>(modalities);
    }

//    @Override
//    public String toString() {
//        return printMultiLine();
//    }

    @Override
    public String toString() {
        return "DicomSegmentation{" +
               "seriesUID='" + seriesUID + "\'\n" +
               ", sopInstanceUID='" + sopInstanceUID + "\'\n" +
               ", seriesNumber=" + seriesNumber + "\'\n" +
               ", segmentationType='" + segmentationType + "\'\n" +
               ", segmentationFractionalType='" + segmentationFractionalType + "\'\n" +
               ", maximumFractionalValue='" + maximumFractionalValue + "\'\n" +
               ", segmentationNumber=" + segmentationNumber + "\'\n" +
               ", segmentLabel='" + segmentLabel + "\'\n" +
               ", segmentDescription='" + segmentDescription + "\'\n" +
               ", segmentAlgorithmType='" + segmentAlgorithmType + "\'\n" +
               ", anatomicRegionCode=" + anatomicRegionCode + "\'\n" +
               ", segmentedPropertyCategoryCode=" + segmentedPropertyCategoryCode + "\'\n" +
               ", segmentedPropertyTypeCode=" + segmentedPropertyTypeCode + "\'\n" +
               ", segmentedPropertyTypeModifierCode=" + segmentedPropertyTypeModifierCode + "\'\n" +
               ", trackingUID='" + trackingUID + "\'\n" +
               ", trackingID='" + trackingID + "\'\n" +
               ", segmentAlgorithmName='" + segmentAlgorithmName + "\'\n" +
               ", instanceNumber='" + instanceNumber + "\'\n" +
               ", contentDate='" + contentDate + "\'\n" +
               ", contentTime='" + contentTime + "\'\n" +
               ", referencedStudyUIDs=" + referencedStudyUIDs + "\'\n" +
               ", referencedSeriesUIDs=" + referencedSeriesUIDs + "\'\n" +
               ", referencedModalities=" + referencedModalities + "\'\n" +
               '}';
    }

    public String getStudyUID() {
        return studyUID;
    }

    public String getSeriesUID() {
        return seriesUID;
    }

    public String getSopInstanceUID() {
        return sopInstanceUID;
    }

    public int getSeriesNumber() {
        return seriesNumber;
    }

    public int getSegmentationNumber() {
        return segmentationNumber;
    }

    public String getSegmentLabel() {
        return segmentLabel;
    }

    public String getSegmentDescription() {
        return segmentDescription;
    }
    
    public DicomCodeValue getAnatomicRegionCode() {
        return anatomicRegionCode;
    }

    public DicomCodeValue getSegmentedPropertyCategoryCode() {
        return segmentedPropertyCategoryCode;
    }

    public DicomCodeValue getSegmentedPropertyTypeCode() {
        return segmentedPropertyTypeCode;
    }

    public DicomCodeValue getSegmentedPropertyTypeModifierCode() {
        return segmentedPropertyTypeModifierCode;
    }

    public String getMaximumFractionalValue() {
        return maximumFractionalValue;
    }
    
    public List<String> getReferencedStudyUIDs() {
        return referencedStudyUIDs;
    }

    public List<String> getReferencedSeriesUIDs() {
        return referencedSeriesUIDs;
    }

    public List<String> getReferencedModalities() {
        return referencedModalities;
    }

    public String getSegmentationType() {
        return segmentationType;
    }

    public String getSegmentationFractionalType() {
        return segmentationFractionalType;
    }

    public String getSegmentAlgorithmType() {
        return segmentAlgorithmType;
    }

    public String getTrackingUID() {
        return trackingUID;
    }

    public String getTrackingID() {
        return trackingID;
    }

    public String getSegmentAlgorithmName() {
        return segmentAlgorithmName;
    }
}
