/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.Main
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.nrg.xnat.dicom.segmentation.app.model.seg.DicomSegmentation;
import org.nrg.xnat.dicom.segmentation.app.model.sr.DicomSRMeasurementReport;
import org.nrg.xnat.dicom.segmentation.services.DicomSegmentationAssessor;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by davidmaffitt on 11/25/16.
 */
public class Main {

    public static void main( String[] args) {
        if( args.length < 5) {
            System.out.println("First arg is console output format for SR, 'text' or 'json'");
            System.out.println("Second arg is the ID of the project containing the associated XNAT imaging session object.");
            System.out.println("Third arg is session ID of the associated XNAT imaging session object.");
            System.out.println("Fourth  arg is path to a file or directory to scan.");
            System.out.println("Fifth arg is path to a directory for output.");
            System.exit(0);
        }
//        File file = new File( args[0]);
//        File file = new File("/Users/davidmaffitt/Desktop/1.2.276.0.7230010.3.1.3.8323329.19117.1440001329.647665/000000.dcm");
        Main main = new Main();

        try {
            String format = args[0];
            String projectId = args[1];
            String sessionId = args[2];
            File in_file = new File(args[3]);
            File out_file = new File(args[4]);
            main.process(format, projectId, sessionId, in_file, out_file);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void process(String format, final String projectId, final String sessionId, File in_file, File out_file) throws Exception {
        if (!out_file.exists()) {
            out_file.mkdirs();
        }
        if (out_file.isFile()) {
            throw new FileNotFoundException("The indicated output path " + out_file.getAbsolutePath() + " exists but is a file, not a folder.");
        }
        if( in_file != null) {
            if( in_file.isDirectory()) {
                final File[] files = in_file.listFiles();
                if (files == null) {
                    throw new FileNotFoundException("The indicated input directory " + in_file.getAbsolutePath() + " contains no files.");
                }
                for( File f: files) {
                    process(format, projectId, sessionId, f, out_file);
                }
            }
            else {
                processDicomObject( format, projectId, sessionId, in_file, out_file);
            }
        }
    }

    protected void processDicomObject(String format, final String projectId, final String sessionId, File in_file, File out_dir) throws Exception {
        try (DicomInputStream dis = new DicomInputStream(in_file)){
            DicomObject dicomObject = dis.readDicomObject();

            String modality = dicomObject.getString(Tag.Modality);

            if( ! out_dir.exists() && ! out_dir.isDirectory()) {
                if( ! out_dir.mkdir()) {
                    System.out.println( "Output directory does not exist and was not created: " + out_dir);
                }
            }

            DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String dateString = df.format(new Date());

            switch (modality) {
                case "SEG":
                    List<DicomSegmentationAssessor> assessors = getDicomSegmentationAssessors( dicomObject, projectId, sessionId);

                    String template = "Found {} segmentation assessors.";
                    String msg = String.format( template, assessors.size());

                    String fileName = "seg" + dateString + ".xml";
                    File outFile = new File( out_dir, fileName);
                    Writer w = new FileWriter(outFile, true);

                    for (final DicomSegmentationAssessor assessor : assessors) {
                        assessor.toXML(w);
                        w.flush();
                    }

                    break;
                case "SR":
                    DicomSRMeasurementReport report = getReport( dicomObject);
                    String report_msg = "Found measurement report with title '{}'.";
                    System.out.println( String.format( report_msg, report.getTitle()));
                    switch (format) {
                        case "text":

                            fileName = "sr" + dateString + ".txt";
                            outFile = new File( out_dir, fileName);
                            PrintStream ps = new PrintStream(outFile);

                            report.write(ps, "text");
                            break;
                        case "json":
                            fileName = "sr" + dateString + ".json";
                            outFile = new File( out_dir, fileName);
                            ps = new PrintStream(outFile);

                            report.write( ps, "json");
                            break;
                    }
                    break;
                default:
                    System.out.println("Skipping DICOM object with modality = " + modality);
                    break;
            }
        }
        catch (IOException e) {
            String template = "Could not read file '{}' as DICOM object. {}.";
            String msg = String.format( template, in_file, e.getMessage());
            System.out.println(msg);
        }
    }

    public List<DicomSegmentationAssessor> getDicomSegmentationAssessors(DicomObject dicomObject, final String projectId, final String sessionId) throws Exception {
        List<DicomSegmentation> dicomSegmentationList = DicomSegmentation.createSegmentations(dicomObject);
        List<DicomSegmentationAssessor> assessors = new ArrayList<>(dicomSegmentationList.size());

        for( DicomSegmentation seg: dicomSegmentationList) {
            System.out.println( "Segmentation number: " + seg.getSegmentationNumber());
            DicomSegmentationAssessor assessor = new DicomSegmentationAssessor( dicomSegmentationList.get(0), projectId, sessionId);
            assessors.add( assessor);
        }
        return assessors;
    }

    public DicomSRMeasurementReport getReport( DicomObject dicomObject) {
        return new DicomSRMeasurementReport( dicomObject);
    }
}
