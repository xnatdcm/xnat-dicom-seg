/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomContentItemText
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomContentItemText extends DicomContentItem {
    private String text;

    public DicomContentItemText(DicomObject dicomObject, String relationship){
        super( dicomObject, relationship);
        text = dicomObject.getString(Tag.TextValue);
    }

    public String getText() {
        return text;
    }

    public String toString() {
        StringBuilder sb1 = new StringBuilder( super.toString());
        StringBuilder sb = new StringBuilder();
        sb.append(" Value: " + text );
        int start = sb1.indexOf("<replace>");
        int stop = start + "<replace>".length();
        sb1.replace( start, stop, sb.toString());
        sb.append("]\n");
        return sb1.toString();
    }

}
