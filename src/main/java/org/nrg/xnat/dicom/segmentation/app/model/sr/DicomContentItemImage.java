/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomContentItemImage
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomContentItemImage extends DicomContentItem {
    private String referencedSOPClassUID;
    private String getReferencedSOPInstanceUID;

    private String[] referencedFrameNumbers;
    private int[] referencedSegmentNumbers;

    public DicomContentItemImage(DicomObject dicomObject, String relationship) {
        super( dicomObject, relationship);
        DicomElement refSOPSeqElement = dicomObject.get( Tag.ReferencedSOPSequence);
        DicomObject item = refSOPSeqElement.getDicomObject();   // only one item shall be included.
        referencedSOPClassUID = item.getString( Tag.ReferencedSOPClassUID);
        getReferencedSOPInstanceUID = item.getString( Tag.ReferencedSOPInstanceUID);

        referencedFrameNumbers = item.getStrings( Tag.ReferencedFrameNumber);
        referencedSegmentNumbers = item.getInts( Tag.ReferencedSegmentNumber);

    }

    public String getReferencedSOPClassUID() {
        return referencedSOPClassUID;
    }

    public String getGetReferencedSOPInstanceUID() {
        return getReferencedSOPInstanceUID;
    }

    public String[] getReferencedFrameNumbers() {
        return referencedFrameNumbers;
    }

    public int[] getReferencedSegmentNumbers() {
        return referencedSegmentNumbers;
    }

    public String toString() {
        StringBuilder sb1 = new StringBuilder( super.toString());
        StringBuilder sb = new StringBuilder();
        sb.append(" SOP Class: '" + referencedSOPClassUID + "' SOPInstanceUID: '" + getReferencedSOPInstanceUID + "'");
        if( referencedFrameNumbers != null) {
            sb.append(" Referenced Frame numbers: '" + referencedFrameNumbers + "'");
        }
        if( referencedSegmentNumbers != null) {
            sb.append(" Referenced Segment Numbers: '" + referencedSegmentNumbers + "'");
        }
        int start = sb1.indexOf("<replace>");
        int stop = start + "<replace>".length();
        sb1.replace( start, stop, sb.toString());
        sb.append("]\n");
        return sb1.toString();
    }
}
