/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.plugin.DicomSegmentationPlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.om.XdcmDicomsegmentation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "xnatDicomSegmentationPlugin", name = "XNAT DICOM Segmentation Plugin",
            entityPackages = "org.nrg.xnat.dicom.segmentation.entities",
            dataModels = {@XnatDataModel(value = XdcmDicomsegmentation.SCHEMA_ELEMENT_NAME,
                                         code = "SEG",
                                         singular = "DICOM Segmentation",
                                         plural = "DICOM Segmentations")})
@ComponentScan({"org.nrg.xnat.dicom.segmentation.repositories", "org.nrg.xnat.dicom.segmentation.services.impl.hibernate"})
public class DicomSegmentationPlugin {
    @Bean
    public String dicomSegmentationPluginMessage() {
        return "Hello there from the DICOM segmentation plugin";
    }
}
