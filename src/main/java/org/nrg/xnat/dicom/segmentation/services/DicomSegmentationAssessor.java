/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.services.DicomSegmentationAssessor
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.services;

import org.dcm4che2.data.DicomObject;
import org.nrg.xdat.bean.XdcmDicomsegmentationBean;
import org.nrg.xdat.bean.XnatDicomcodedvalueBean;
import org.nrg.xnat.dicom.segmentation.app.model.seg.DicomSegmentation;

import java.io.IOException;
import java.io.Writer;

public class DicomSegmentationAssessor {
    private XdcmDicomsegmentationBean segAssessor = new XdcmDicomsegmentationBean();

    public DicomSegmentationAssessor(DicomSegmentation dicomSegmentation, final String projectId, final String sessionId) throws Exception {
        segAssessor = new XdcmDicomsegmentationBean();

        segAssessor.setLabel(sessionId + "_SEG_" + dicomSegmentation.getSeriesNumber());
        segAssessor.setProject(projectId);
        segAssessor.setImagesessionId(sessionId);
        segAssessor.setSeriesid( Integer.toString(dicomSegmentation.getSeriesNumber()));
        segAssessor.setSegmentid( dicomSegmentation.getSegmentationNumber());
        segAssessor.setLabel( dicomSegmentation.getSegmentLabel());
        if(dicomSegmentation.getSegmentDescription() != null)
            segAssessor.setSegmentdescription( dicomSegmentation.getSegmentDescription());

        if( "BINARY".equals( dicomSegmentation.getSegmentationType())) {
            segAssessor.setSegmentationtype_encoding("BINARY");
        }
        else {
            segAssessor.setSegmentationtype_encoding( dicomSegmentation.getSegmentationFractionalType());
//            segAssessor.setSegmentationtype_maximalfractionalvalue( dicomSegmentation.getMaximumFractionalValue());
        }

        segAssessor.setAlgorithm_type( dicomSegmentation.getSegmentAlgorithmType());

        for( String studyUID: dicomSegmentation.getReferencedStudyUIDs()) {
            segAssessor.setAcquiredstudyid( studyUID);
        }

        segAssessor.setTrackingid( dicomSegmentation.getTrackingID());
        segAssessor.setTrackinguid( dicomSegmentation.getTrackingUID());

        if( dicomSegmentation.getReferencedSeriesUIDs().size() > 0)
            segAssessor.setAcquiredseriesid( dicomSegmentation.getReferencedSeriesUIDs().get(0));

        if( dicomSegmentation.getReferencedModalities().size() > 0)
            segAssessor.setAcquiredstudymodality( dicomSegmentation.getReferencedModalities().get(0));

//        segAssessor.setAlgorithm();

        XnatDicomcodedvalueBean cv = new XnatDicomcodedvalueBean();
        cv.setMeaning( dicomSegmentation.getAnatomicRegionCode().getCodeMeaning());
        cv.setValue( dicomSegmentation.getAnatomicRegionCode().getCodeValue());
        cv.setDesignator( dicomSegmentation.getAnatomicRegionCode().getCodeDesignator());
        segAssessor.setAnatomicregion( cv);

        cv = new XnatDicomcodedvalueBean();
        cv.setMeaning( dicomSegmentation.getSegmentedPropertyCategoryCode().getCodeMeaning());
        cv.setValue( dicomSegmentation.getSegmentedPropertyCategoryCode().getCodeValue());
        cv.setDesignator( dicomSegmentation.getSegmentedPropertyCategoryCode().getCodeDesignator());
        segAssessor.setSegmentedproperty_category( cv);

        cv = new XnatDicomcodedvalueBean();
        cv.setMeaning( dicomSegmentation.getSegmentedPropertyTypeCode().getCodeMeaning());
        cv.setValue( dicomSegmentation.getSegmentedPropertyTypeCode().getCodeValue());
        cv.setDesignator( dicomSegmentation.getSegmentedPropertyTypeCode().getCodeDesignator());
        segAssessor.setSegmentedproperty_type( cv);

//        segAssessor.setSegmentationtype_encoding( String);

    }

    public void toXML( Writer w) throws IOException {
        segAssessor.toXML(w, true);
    }

    public void process(final DicomObject dicom) throws Exception {

//        final XdcmDicomsegmentation segmentation = new XdcmDicomsegmentation();
//        segmentation.setId("seg1");
//        segmentation.setSegmentid("1");
//        segmentation.setSegmentationtype_encoding(dicom.getString(Tag.SegmentationType));
//        segmentation.setSegmentationtype_maximalfractionalvalue(dicom.getInt(Tag.MaximumFractionalValue));
//
//        final UserI user = XDAT.getUserDetails();
//        SaveItemHelper.authorizedSave(segmentation, user, false, false, EventUtils.ADMIN_EVENT(user));

//        PopulateItem populater = PopulateItem.Populate(data, XdcmDicomsegmentation.SCHEMA_ELEMENT_NAME, true);
//        ItemI found = populater.getItem();
//        if (found instanceof XFTItem) {
//            final XFTItem item = (XFTItem) found;
//            SaveItemHelper.authorizedSave(item, user, false, false, EventUtils.ADMIN_EVENT(user));
//        }
    }

}
