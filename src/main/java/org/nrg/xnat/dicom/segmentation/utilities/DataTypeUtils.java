/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.utilities.DataTypeUtils
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.utilities;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatImageassessordataI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XdcmDicomsegmentation;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatSegscandata;
import org.nrg.xdat.om.XnatSrscandata;
import org.nrg.xnat.dicom.segmentation.entities.Measurements;
import org.nrg.xnat.dicom.segmentation.services.MeasurementsService;

import java.util.List;

public class DataTypeUtils {
    public static void checkForUnprocessedSegmentations(final Context context) {
        final List<String> segAndSrScans = Lists.newArrayList();
        if (context.get("om") != null) {
            final XnatImagesessiondata session = (XnatImagesessiondata) context.get("om");
            if (session != null) {
                for (final XnatImagescandataI scan : session.getScans_scan()) {
                    if (StringUtils.equals(scan.getXSIType(), XnatSegscandata.SCHEMA_ELEMENT_NAME) ||
                        StringUtils.equals(scan.getXSIType(), XnatSrscandata.SCHEMA_ELEMENT_NAME)) {
                        segAndSrScans.add(scan.getId());
                    }
                }

                if (!segAndSrScans.isEmpty()) {
                    final MeasurementsService service = XDAT.getContextService().getBean(MeasurementsService.class);
                    for (final XnatImageassessordataI assessor : session.getAssessors()) {
                        if (assessor instanceof XdcmDicomsegmentation) {
                            final XdcmDicomsegmentation segmentation = (XdcmDicomsegmentation) assessor;
                            if (segAndSrScans.contains(segmentation.getAcquiredseriesid())) {
                                segAndSrScans.remove(segmentation.getAcquiredseriesid());
                            }
                            final Measurements measurements = service.getSegmentationMeasurements(assessor.getId());
                            if (measurements != null) {
                                segAndSrScans.remove(measurements.getStructuredReportImageScanId());
                            }
                        }
                    }
                }
            }
        }

        context.put("hasUnprocessedSegmentations", segAndSrScans.size() > 0);
        context.put("scanIds", Joiner.on(",").join(segAndSrScans));
    }
}
