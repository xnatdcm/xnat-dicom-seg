/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.services.MeasurementsService
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnat.dicom.segmentation.entities.Measurements;

/**
 * Manages {@link Measurements DICOM segmentation measurements objects}.
 */
public interface MeasurementsService extends BaseHibernateService<Measurements> {
    Measurements getSegmentationMeasurements(String dicomSegmentationId);
}
