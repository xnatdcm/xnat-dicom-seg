/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.DicomContentItem
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.xnat.dicom.segmentation.app.model.DicomCodeValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomContentItem {
    private DicomCodeValue conceptNameCode; // 1c
    private DicomCodeValue conceptCode;
    private String valueType;  // type 1
    private String relationship;   // relationship from parent.
    List<DicomContentItem> kids;

    private DicomContentItem() {}

    public static DicomContentItem createInstance( DicomObject dicomObject, String relationship) {
        String contentType = dicomObject.getString(0x0040A040);
        DicomContentItem contentItem = null;

        switch (contentType) {
            case "CONTAINER":
                contentItem = new DicomContentItemContainer(dicomObject, null);
                break;
            case "TEXT":
                contentItem = new DicomContentItemText( dicomObject, relationship);
                break;
            case "DATE":
                contentItem = new DicomContentItemDate( dicomObject, relationship);
                break;
            case "TIME":
                contentItem = new DicomContentItemTime( dicomObject, relationship);
                break;
            case "NUM":
                contentItem = new DicomContentItemNumBroken( dicomObject, relationship);
                break;
            case "IMAGE":
                contentItem = new DicomContentItemImage( dicomObject, relationship);
                break;
            case "COMPOSITE":
                contentItem = new DicomContentItemComposite( dicomObject, relationship);
                break;
            case "UIDREF":
                contentItem = new DicomContentItemUIDRef( dicomObject, relationship);
                break;
            case "CODE":
                contentItem = new DicomContentItemCode( dicomObject, relationship);
                break;
            default:
                contentItem = new DicomContentItem( dicomObject, relationship);

        }
        return contentItem;
    }

    protected DicomContentItem(DicomObject dicomObject, String relationship) {
        this.conceptNameCode = getCodedValue( dicomObject, 0x0040A043);
        this.conceptCode = new DicomCodeValue( dicomObject, Tag.ConceptCodeSequence);
        this.valueType = dicomObject.getString( 0x0040A040);
        this.relationship = relationship;
        kids = new ArrayList<>();
        DicomElement de = dicomObject.get(0x0040A730);
        if( de != null) {
            for( int i = 0; i < de.countItems(); i++) {
                DicomObject item = de.getDicomObject(i);
                String relationshipType = item.getString(Tag.RelationshipType);
                kids.add(  DicomContentItem.createInstance( item, relationshipType));
            }
        }
    }

    private DicomCodeValue getCodedValue(DicomObject dicomObject, int tag) {
        DicomCodeValue dicomCodeValue = null;
        DicomElement de = dicomObject.get( tag);
        if( de != null && de.hasItems()) {
            DicomObject item = de.getDicomObject(0);
            dicomCodeValue = new DicomCodeValue( item.getString(0x00080100), item.getString(0x00080102), item.getString(0x00080104));
        }
        return dicomCodeValue;
    }

    /**
     * Add <replace> string so subclasses can insert content into middle of output.
     *
     * @return
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if( relationship != null) {
            sb.append( relationship + " ");
        }
        sb.append( valueType + " [");
        sb.append(" Concept Name: " + conceptNameCode);
        sb.append(" Concept: " + conceptCode );
        sb.append("<replace>" );
        for( int i = 0; i < kids.size(); i++) {
            sb.append( "\n\t" + kids.get(i));
        }
        sb.append("]\n");
        return sb.toString();
    }

    public DicomCodeValue getConceptNameCode() {
        return conceptNameCode;
    }

    public DicomCodeValue getConceptCode() {
        return conceptCode;
    }

    public String getValueType() {
        return valueType;
    }

    public List<DicomContentItem> getKids() {
        return kids;
    }
}
