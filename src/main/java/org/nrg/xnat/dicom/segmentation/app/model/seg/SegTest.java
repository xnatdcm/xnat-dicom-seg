/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.seg.SegTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.seg;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomInputStream;
import org.nrg.xnat.dicom.segmentation.services.DicomSegmentationAssessor;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by davidmaffitt on 11/19/16.
 */
public class SegTest {

    public static void main( String[] args) {
        try {
            File file = new File("/Users/davidmaffitt/Documents/dicomqi/data/testdata-2/SEG/3DSlicer/tumor_User1_Manual_Trial1.dcm");
            DicomInputStream dis = new DicomInputStream( file);

            DicomObject dicomObject = dis.readDicomObject();

            List<DicomSegmentation> dicomSegmentationList = DicomSegmentation.createSegmentations(dicomObject);

            if( dicomSegmentationList.isEmpty()) {
                System.out.println( "No segmentations in " + file);
            }
            else {
                for( DicomSegmentation seg: dicomSegmentationList) {
                    System.out.println( "Segmentation number: " + seg.getSegmentationNumber());
                    System.out.println( seg);
                }

//                DicomSegmentationAssessor assessor = new DicomSegmentationAssessor( dicomSegmentationList.get(0));
            }
        }
        catch( IOException e) {
            e.printStackTrace();
        }
    }
}
