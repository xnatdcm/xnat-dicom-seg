/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.DicomCodeValue
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 * Created by davidmaffitt on 11/16/16.
 */
public class DicomCodeValue {
    private String codeDesignator;
    private String codeMeaning;
    private String codeValue;

    /**
     * Construct the code value when dicomobject is the parent with a single item in the sequence tag.
     *
     * @param dicomObject
     * @param tag
     */
    public DicomCodeValue( DicomObject dicomObject, int tag) {
        DicomElement de = dicomObject.get(tag);
        if( de != null && de.hasItems()) {
            DicomObject item = de.getDicomObject();
            codeDesignator = (item == null) ? null : item.getString(Tag.CodeLabel);
            codeMeaning = (item == null) ? null : item.getString(Tag.CodeMeaning);
            codeValue = (item == null) ? null : item.getString(Tag.CodeValue);
        }
    }

    /**
     * Construct the code value where dicomobject is the object containing the code.
     *
     * @param dicomObject
     */
    public DicomCodeValue( DicomObject dicomObject) {
        this.codeDesignator = dicomObject.getString( Tag.CodeLabel);
        this.codeMeaning = dicomObject.getString( Tag.CodeMeaning);
        this.codeValue = dicomObject.getString( Tag.CodeValue);
    }

    public DicomCodeValue(String codeDesignator, String codeMeaning, String codeValue) {
        this.codeDesignator = codeDesignator;
        this.codeMeaning = codeMeaning;
        this.codeValue = codeValue;
    }

    public String getCodeDesignator() {
        return codeDesignator;
    }

    public void setCodeDesignator(String codeDesignator) {
        this.codeDesignator = codeDesignator;
    }

    public String getCodeMeaning() {
        return codeMeaning;
    }

    public void setCodeMeaning(String codeMeaning) {
        this.codeMeaning = codeMeaning;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[Designator: " + codeDesignator);
        sb.append(", Meaning: " + codeMeaning);
        sb.append(", Value: " + codeValue+"]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DicomCodeValue that = (DicomCodeValue) o;

        if (codeDesignator != null ? !codeDesignator.equals(that.codeDesignator) : that.codeDesignator != null)
            return false;
        return codeValue != null ? codeValue.equals(that.codeValue) : that.codeValue == null;

    }

    @Override
    public int hashCode() {
        int result = codeDesignator != null ? codeDesignator.hashCode() : 0;
        result = 31 * result + (codeValue != null ? codeValue.hashCode() : 0);
        return result;
    }
}
