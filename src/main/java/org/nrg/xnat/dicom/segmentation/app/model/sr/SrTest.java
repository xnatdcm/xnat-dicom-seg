/*
 * xnat-dicom-seg: org.nrg.xnat.dicom.segmentation.app.model.sr.SrTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.dicom.segmentation.app.model.sr;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomInputStream;

import java.io.File;
import java.io.IOException;

public class SrTest {

    public static void main(String[] args) {
        try {
            File file = new File("/Users/davidmaffitt/Desktop/1.2.276.0.7230010.3.1.3.8323329.19117.1440001329.647665/000000.dcm");
            DicomInputStream dis = new DicomInputStream( file);

            DicomObject dicomObject = dis.readDicomObject();

            DicomContentItem dicomContentItem = DicomContentItem.createInstance( dicomObject, null);

            System.out.println("Content: " + dicomContentItem);

            ObjectMapper mapper = new ObjectMapper();

            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dicomContentItem);
//            System.out.println("JSON Content: " + jsonInString);

            mapper.writerWithDefaultPrettyPrinter().writeValue( new File("/tmp/json.txt"), dicomContentItem);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
